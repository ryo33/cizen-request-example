defmodule API do
  @categories %{
    "Elixir" => "01",
    "Nature" => "02",
    "Cizen" => "03"
  }

  @articles %{
    "01" => ["Let's Try Elixir", "Implement an API with Elixir"],
    "02" => ["Beautiful Nature"],
    "03" => ["Getting Started with Cizen", "Asynchronous Request with Cizen"]
  }

  def get_category_id(text) do
    :timer.sleep(:rand.uniform(10) * 50) # Delay for an API call
    @categories[text]
  end

  def get_articles(category) do
    :timer.sleep(:rand.uniform(10) * 50) # Delay for an API call
    @articles[category]
  end
end

defmodule Connection do
  defmodule Request do
    defstruct [:connection_id, :type, :body]

    use Cizen.Request
    defresponse Response, :request_id do
      defstruct [:request_id, :body]
    end
  end

  use Cizen.Automaton
  alias Cizen.{Event, Filter}
  alias Cizen.Effects.{Dispatch, Receive, Subscribe}

  defstruct []

  def spawn(id, %__MODULE__{}) do
    perform id, %Subscribe{
      event_filter: Filter.new(
        fn %Event{body: %Connection.Request{connection_id: value}} -> value == id end
      )
    }

    :loop
  end

  def yield(id, :loop) do
    request_event = perform id, %Receive{}
    %Request{type: type, body: body} = request_event.body

    # Calls the API
    response = apply(API, type, [body])

    perform id, %Dispatch{
      body: %Connection.Request.Response{
        request_id: request_event.id,
        body: response
      }
    }

    :loop
  end
end

defmodule RequestLogger do
  use Cizen.Automaton
  alias Cizen.{Event, Filter}
  alias Cizen.Effects.{All, Receive, Subscribe}
  alias Cizen.StartSaga

  defstruct []

  def spawn(id, %__MODULE__{}) do
    perform id, %All{effects: [
      %Subscribe{
        event_filter: Filter.new(
          fn %Event{body: %StartSaga{saga: %Connection{}}} -> true end
        )
      },
      %Subscribe{
        event_filter: Filter.new(
          fn %Event{body: %Connection.Request{}} -> true end
        )
      },
      %Subscribe{
        event_filter: Filter.new(
          fn %Event{body: %Connection.Request.Response{}} -> true end
        )
      },
    ]}

    %{
      connections: %{}, # connection ID => connection number
      requests: %{} # request ID => connection number
    }
  end

  defp tabs(n), do: Stream.cycle([" "]) |> Enum.take(20*n) |> Enum.join()
  def yield(id, state) do
    event = perform id, %Receive{}
    case event.body do
      %StartSaga{id: connection_id} ->
        put_in(state.connections[connection_id], Map.size(state.connections))
      %Connection.Request{connection_id: connection_id} = request ->
        connection_number = state.connections[connection_id]
        IO.puts("""
        #{tabs(connection_number)}request
        #{tabs(connection_number)}#{request.type}
        #{tabs(connection_number)}body: #{request.body}
        """)
        request_id = event.id
        put_in(state.requests[request_id], connection_number)
      %Connection.Request.Response{request_id: request_id} = response ->
        {connection_number, state} = pop_in(state.requests[request_id])
        IO.puts("""
        #{tabs(connection_number)}response
        #{tabs(connection_number)}#{inspect response.body}
        """)
        state
    end
  end
end

defmodule ConnectionPool do
  defmodule BorrowConnection do
    defstruct []
    use Cizen.Request
    defresponse Lend, :borrow_id do
      defstruct [:borrow_id, :connection_id]
    end
  end

  defmodule PushConnection, do: defstruct [:connection_id]

  use Cizen.Automaton
  alias Cizen.{Event, Filter}
  defstruct []

  alias Cizen.Effects.{All, Dispatch, Receive, Subscribe}

  def spawn(id, %__MODULE__{}) do
    perform id, %All{effects: [
      %Subscribe{
        event_filter: Filter.new(fn %Event{body: %BorrowConnection{}} -> true end)
      },
      %Subscribe{
        event_filter: Filter.new(fn %Event{body: %PushConnection{}} -> true end)
      }
    ]}

    :loop
  end

  def yield(id, :loop) do
    borrow_event = perform id, %Receive{
      event_filter: Filter.new(fn %Event{body: %BorrowConnection{}} -> true end)
    }

    push_event = perform id, %Receive{
      event_filter: Filter.new(fn %Event{body: %PushConnection{}} -> true end)
    }
    %PushConnection{connection_id: connection_id} = push_event.body

    perform id, %Dispatch{
      body: %BorrowConnection.Lend{
        borrow_id: borrow_event.id,
        connection_id: connection_id
      }
    }

    :loop
  end
end

defmodule Main do
  use Cizen.Effectful

  def single_connection do
    alias Cizen.Effects.{Request, Start}

    connection_id = handle fn id ->
      perform id, %Start{saga: %Connection{}}
    end
    ["Elixir", "Nature", "Cizen"]
    |> Enum.map(fn category ->
      fn ->
        handle fn id ->
          # Request to get a category ID
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_category_id,
            body: category
          }}
          %Connection.Request.Response{body: category_id} = response_event.body

          # Request to get articles
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_articles,
            body: category_id
          }}
          %Connection.Request.Response{body: articles} = response_event.body

          # Returns the articles
          articles
        end
      end
    end)
    |> Enum.map(&Task.async(&1))
    |> Enum.map(&Task.await(&1, :infinity))
    |> Enum.map(&IO.inspect(&1))
  end

  def single_connection_with_logger do
    alias Cizen.Effects.{Request, Start}

    connection_id = handle fn id ->
      perform id, %Start{saga: %RequestLogger{}}
      perform id, %Start{saga: %Connection{}}
    end
    ["Elixir", "Nature", "Cizen"]
    |> Enum.map(fn category ->
      fn ->
        handle fn id ->
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_category_id,
            body: category
          }}
          %Connection.Request.Response{body: category_id} = response_event.body
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_articles,
            body: category_id
          }}
          %Connection.Request.Response{body: articles} = response_event.body
          articles
        end
      end
    end)
    |> Enum.map(&Task.async(&1))
    |> Enum.map(&Task.await(&1, :infinity))
  end

  def connection_pool_ugly(size) do
    alias Cizen.Effects.{Dispatch, Request, Start}

    handle fn id ->
      perform id, %Start{saga: %RequestLogger{}}
      perform id, %Start{saga: %ConnectionPool{}}
      # Starts connections
      for _ <- 1..size do
        connection_id = perform id, %Start{saga: %Connection{}}
        perform id, %Dispatch{
          body: %ConnectionPool.PushConnection{connection_id: connection_id}
        }
      end
    end

    ["Elixir", "Nature", "Cizen"]
    |> Enum.map(fn category ->
      fn ->
        handle fn id ->
          # Borrow a connection
          lend_event = perform id, %Request{body: %ConnectionPool.BorrowConnection{}}
          connection_id = lend_event.body.connection_id
          # Request
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_category_id,
            body: category
          }}
          %Connection.Request.Response{body: category_id} = response_event.body
          # Return the connection
          perform id, %Dispatch{
            body: %ConnectionPool.PushConnection{connection_id: connection_id}
          }

          # Borrow a connection
          lend_event = perform id, %Request{body: %ConnectionPool.BorrowConnection{}}
          connection_id = lend_event.body.connection_id
          # Request
          response_event = perform id, %Request{body: %Connection.Request{
            connection_id: connection_id,
            type: :get_articles,
            body: category_id
          }}
          %Connection.Request.Response{body: articles} = response_event.body
          # Return the connection
          perform id, %Dispatch{
            body: %ConnectionPool.PushConnection{connection_id: connection_id}
          }

          articles
        end
      end
    end)
    |> Enum.map(&Task.async(&1))
    |> Enum.map(&Task.await(&1))
  end

  defp start_connection do
    alias Cizen.Effects.{Chain, Dispatch, Start}
    %Chain{effects: [
      %Start{saga: %Connection{}},
      fn connection_id ->
        %Dispatch{
          body: %ConnectionPool.PushConnection{connection_id: connection_id}
        }
      end
    ]}
  end

  defp request(type, body) do
    alias Cizen.Effects.{Chain, Dispatch, Map, Request}
    effect = %Chain{effects: [
      %Request{body: %ConnectionPool.BorrowConnection{}},
      fn lend_event ->
        connection_id = lend_event.body.connection_id
        %Request{body: %Connection.Request{
          connection_id: connection_id,
          type: type,
          body: body
        }}
      end,
      fn lend_event, _ ->
        connection_id = lend_event.body.connection_id
        %Dispatch{
          body: %ConnectionPool.PushConnection{connection_id: connection_id}
        }
      end
    ]}
    %Map{
      effect: effect,
      transform: fn [_, response_event, _] -> response_event end
    }
  end

  def connection_pool(size) do
    alias Cizen.Effects.Start

    handle fn id ->
      perform id, %Start{saga: %RequestLogger{}}
      perform id, %Start{saga: %ConnectionPool{}}
      # Starts connections
      for _ <- 1..size do
        perform id, start_connection()
      end
    end

    ["Elixir", "Nature", "Cizen"]
    |> Enum.map(fn category ->
      fn ->
        handle fn id ->
          response_event = perform id, request(:get_category_id, category)
          %Connection.Request.Response{body: category_id} = response_event.body

          response_event = perform id, request(:get_articles, category_id)
          %Connection.Request.Response{body: articles} = response_event.body

          articles
        end
      end
    end)
    |> Enum.map(&Task.async(&1))
    |> Enum.map(&Task.await(&1, :infinity))
  end
end

case System.argv() do
  ["connection_pool", pool] -> Main.connection_pool(String.to_integer(pool))
  ["single_connection"] -> Main.single_connection()
  ["single_connection_with_logger"] -> Main.single_connection_with_logger()
  _ -> Main.single_connection()
end
